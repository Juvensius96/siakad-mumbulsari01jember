@extends('layouts.app')


@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>

<div class="  ">
                    <div class="row pb-4">
                    <div class="col-6 col-lg-3 mb-3">
                        <div class="card  ">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        {{--  <i class="fas fa-leaf text-info" style="font-size: 40px;"></i>  --}}
                                    </div>
                                    <div class="align-self-center ">
                                        <h6 class=" m-t-10 m-b-0">Total Siswa</h6>
                                        <h2 class="m-t-0 "> {{ $count_murid }}</h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 mb-3">
                        <div class="card ">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        {{--  <i class="fas fa-credit-card text-danger" style="font-size: 40px;"></i>  --}}
                                    </div>
                                    <div class="align-self-center">
                                        <h6 class="text-muted m-t-10 m-b-0">Total PTK</h6>
                                        <h2 class="m-t-0"> {{ $count_ptk }} </h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-lg-3 mb-3">
                        <div class="card ">
                            <div class="card-body ">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center">
                                        {{--  <i class="icon-badge text-primary" style="font-size: 40px;"></i>  --}}
                                    </div>
                                    <div class="align-self-center">
                                        <h6 class=" m-t-10 m-b-0">Total DNT</h6>
                                        <h2 class="m-t-0"> {{ $count_dnt }}</h2></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class=" col-6 col-lg-3 mb-3">
                        <div class="card ">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="m-r-20 align-self-center"> 
                                        {{--  <i class="fas fa-users text-warning" style="font-size: 40px;"></i>  --}}
                                    </div>
                                    <div class="align-self-center">
                                        <h6 class="text-muted m-t-10 m-b-0">Total Kelulusan</h6>
                                        <h2 class="m-t-0"> {{ $count_kelulusan }}</h2></div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
<!-- ============================================================== -->
                <!-- Twitter facebook and mail boxes -->
                <!-- ============================================================== -->
                {{--  <div class="row pb-4">
                    <div class="col-lg-4">
                        <div class="card bg-info text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="stats">
                                        <h1 class="text-white">3257+</h1>
                                        <h6 class="text-white">Twitter Followers</h6>
                                        <button class="btn btn-rounded btn-outline btn-light m-t-10 font-14">Check list</button>
                                    </div>
                                    <div class="stats-icon text-right ml-auto"><i class="fab fa-twitter display-5 op-3 text-dark"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card bg-primary text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="stats">
                                        <h1 class="text-white">6509+</h1>
                                        <h6 class="text-white">Facebook Likes</h6>
                                        <button class="btn btn-rounded btn-outline btn-light m-t-10 font-14">Check list</button>
                                    </div>
                                    <div class="stats-icon text-right ml-auto"><i class="fab fa-facebook-f display-5 op-3 text-dark"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card bg-success text-white">
                            <div class="card-body">
                                <div class="d-flex">
                                    <div class="stats">
                                        <h1 class="text-white">9062+</h1>
                                        <h6 class="text-white">Subscribe</h6>
                                        <button class="btn btn-rounded btn-outline btn-light m-t-10 font-14">Check list</button>
                                    </div>
                                    <div class="stats-icon text-right ml-auto"><i class="fa fa-envelope display-5 op-3 text-dark"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  --}}
</div>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>

<script type="text/javascript">
  // Data retrieved from http://vikjavev.no/ver/index.php?spenn=2d&sluttid=16.06.2015.
$(function () {
    Highcharts.chart('chartdiv', {
    chart: {
        type: 'areaspline'
    },
    title: {
        text: 'Monthly Statistic'
    },
    legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 150,
        y: 100,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF'
    },
    xAxis: {
        categories: [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday'
        ],
        plotBands: [{ // visualize the weekend
            from: 4.5,
            to: 6.5,
            color: 'rgba(68, 170, 213, .2)'
        }]
    },
    yAxis: {
        title: {
            text: 'Pegawai'
        }
    },
    tooltip: {
        shared: true,
        valueSuffix: ' Orang'
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        areaspline: {
            fillOpacity: 0.5
        }
    },
    series: [{
        name: 'Kehadiran',
        data: [3, 4, 3, 5, 4, 10, 12]
    }, {
        name: 'Alpa',
        data: [1, 3, 4, 3, 3, 5, 4]
    }
    , {
        name: 'Cuti',
        data: [4, 5,1, 6, 12, 3, 1]
    }]
});

});



</script>            
@stop