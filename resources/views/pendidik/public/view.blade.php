<div class="container" class="pt-3 pb-3">
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-container" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama', (isset($fields['Nama']['language'])? $fields['Nama']['language'] : array())) }}</td>
						<td>{{ $row->Nama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NUPTK', (isset($fields['NUPTK']['language'])? $fields['NUPTK']['language'] : array())) }}</td>
						<td>{{ $row->NUPTK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JK', (isset($fields['JK']['language'])? $fields['JK']['language'] : array())) }}</td>
						<td>{{ $row->JK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tempat Lahir', (isset($fields['Tempat_Lahir']['language'])? $fields['Tempat_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tempat_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tanggal Lahir', (isset($fields['Tanggal_Lahir']['language'])? $fields['Tanggal_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tanggal_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NIP', (isset($fields['NIP']['language'])? $fields['NIP']['language'] : array())) }}</td>
						<td>{{ $row->NIP}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status Kepegawaian', (isset($fields['Status_Kepegawaian']['language'])? $fields['Status_Kepegawaian']['language'] : array())) }}</td>
						<td>{{ $row->Status_Kepegawaian}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenis PTK', (isset($fields['Jenis_PTK']['language'])? $fields['Jenis_PTK']['language'] : array())) }}</td>
						<td>{{ $row->Jenis_PTK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gelar Depan', (isset($fields['Gelar_Depan']['language'])? $fields['Gelar_Depan']['language'] : array())) }}</td>
						<td>{{ $row->Gelar_Depan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gelar Belakang', (isset($fields['Gelar_Belakang']['language'])? $fields['Gelar_Belakang']['language'] : array())) }}</td>
						<td>{{ $row->Gelar_Belakang}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenjang', (isset($fields['Jenjang']['language'])? $fields['Jenjang']['language'] : array())) }}</td>
						<td>{{ $row->Jenjang}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jurusan Prodi', (isset($fields['Jurusan_Prodi']['language'])? $fields['Jurusan_Prodi']['language'] : array())) }}</td>
						<td>{{ $row->Jurusan_Prodi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sertifikasi', (isset($fields['Sertifikasi']['language'])? $fields['Sertifikasi']['language'] : array())) }}</td>
						<td>{{ $row->Sertifikasi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('TMT Kerja', (isset($fields['TMT_Kerja']['language'])? $fields['TMT_Kerja']['language'] : array())) }}</td>
						<td>{{ $row->TMT_Kerja}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tugas Tambahan', (isset($fields['Tugas_Tambahan']['language'])? $fields['Tugas_Tambahan']['language'] : array())) }}</td>
						<td>{{ $row->Tugas_Tambahan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mengajar', (isset($fields['Mengajar']['language'])? $fields['Mengajar']['language'] : array())) }}</td>
						<td>{{ $row->Mengajar}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jam Tugas Tambahan', (isset($fields['Jam_Tugas_Tambahan']['language'])? $fields['Jam_Tugas_Tambahan']['language'] : array())) }}</td>
						<td>{{ $row->Jam_Tugas_Tambahan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JJM', (isset($fields['JJM']['language'])? $fields['JJM']['language'] : array())) }}</td>
						<td>{{ $row->JJM}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Total JJM', (isset($fields['Total_JJM']['language'])? $fields['Total_JJM']['language'] : array())) }}</td>
						<td>{{ $row->Total_JJM}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Siswa', (isset($fields['Siswa']['language'])? $fields['Siswa']['language'] : array())) }}</td>
						<td>{{ $row->Siswa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kompetensi', (isset($fields['Kompetensi']['language'])? $fields['Kompetensi']['language'] : array())) }}</td>
						<td>{{ $row->Kompetensi}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	