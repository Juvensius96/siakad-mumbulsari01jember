

		 {!! Form::open(array('url'=>'pendidik', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> m_pendidik</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Nama" class=" control-label col-md-4 "> Nama </label>
										<div class="col-md-8">
										  <input  type='text' name='Nama' id='Nama' value='{{ $row['Nama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="NUPTK" class=" control-label col-md-4 "> NUPTK </label>
										<div class="col-md-8">
										  <input  type='text' name='NUPTK' id='NUPTK' value='{{ $row['NUPTK'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="JK" class=" control-label col-md-4 "> JK </label>
										<div class="col-md-8">
										  <input  type='text' name='JK' id='JK' value='{{ $row['JK'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tempat Lahir" class=" control-label col-md-4 "> Tempat Lahir </label>
										<div class="col-md-8">
										  <input  type='text' name='Tempat_Lahir' id='Tempat_Lahir' value='{{ $row['Tempat_Lahir'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tanggal Lahir" class=" control-label col-md-4 "> Tanggal Lahir </label>
										<div class="col-md-8">
										  <input  type='text' name='Tanggal_Lahir' id='Tanggal_Lahir' value='{{ $row['Tanggal_Lahir'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="NIP" class=" control-label col-md-4 "> NIP </label>
										<div class="col-md-8">
										  <input  type='text' name='NIP' id='NIP' value='{{ $row['NIP'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Status Kepegawaian" class=" control-label col-md-4 "> Status Kepegawaian </label>
										<div class="col-md-8">
										  <input  type='text' name='Status_Kepegawaian' id='Status_Kepegawaian' value='{{ $row['Status_Kepegawaian'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jenis PTK" class=" control-label col-md-4 "> Jenis PTK </label>
										<div class="col-md-8">
										  <input  type='text' name='Jenis_PTK' id='Jenis_PTK' value='{{ $row['Jenis_PTK'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Gelar Depan" class=" control-label col-md-4 "> Gelar Depan </label>
										<div class="col-md-8">
										  <input  type='text' name='Gelar_Depan' id='Gelar_Depan' value='{{ $row['Gelar_Depan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Gelar Belakang" class=" control-label col-md-4 "> Gelar Belakang </label>
										<div class="col-md-8">
										  <input  type='text' name='Gelar_Belakang' id='Gelar_Belakang' value='{{ $row['Gelar_Belakang'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jenjang" class=" control-label col-md-4 "> Jenjang </label>
										<div class="col-md-8">
										  <input  type='text' name='Jenjang' id='Jenjang' value='{{ $row['Jenjang'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jurusan Prodi" class=" control-label col-md-4 "> Jurusan Prodi </label>
										<div class="col-md-8">
										  <input  type='text' name='Jurusan_Prodi' id='Jurusan_Prodi' value='{{ $row['Jurusan_Prodi'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sertifikasi" class=" control-label col-md-4 "> Sertifikasi </label>
										<div class="col-md-8">
										  <input  type='text' name='Sertifikasi' id='Sertifikasi' value='{{ $row['Sertifikasi'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="TMT Kerja" class=" control-label col-md-4 "> TMT Kerja </label>
										<div class="col-md-8">
										  <input  type='text' name='TMT_Kerja' id='TMT_Kerja' value='{{ $row['TMT_Kerja'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tugas Tambahan" class=" control-label col-md-4 "> Tugas Tambahan </label>
										<div class="col-md-8">
										  <input  type='text' name='Tugas_Tambahan' id='Tugas_Tambahan' value='{{ $row['Tugas_Tambahan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Mengajar" class=" control-label col-md-4 "> Mengajar </label>
										<div class="col-md-8">
										  <input  type='text' name='Mengajar' id='Mengajar' value='{{ $row['Mengajar'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jam Tugas Tambahan" class=" control-label col-md-4 "> Jam Tugas Tambahan </label>
										<div class="col-md-8">
										  <input  type='text' name='Jam_Tugas_Tambahan' id='Jam_Tugas_Tambahan' value='{{ $row['Jam_Tugas_Tambahan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="JJM" class=" control-label col-md-4 "> JJM </label>
										<div class="col-md-8">
										  <input  type='text' name='JJM' id='JJM' value='{{ $row['JJM'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Total JJM" class=" control-label col-md-4 "> Total JJM </label>
										<div class="col-md-8">
										  <input  type='text' name='Total_JJM' id='Total_JJM' value='{{ $row['Total_JJM'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Siswa" class=" control-label col-md-4 "> Siswa </label>
										<div class="col-md-8">
										  <input  type='text' name='Siswa' id='Siswa' value='{{ $row['Siswa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kompetensi" class=" control-label col-md-4 "> Kompetensi </label>
										<div class="col-md-8">
										  <input  type='text' name='Kompetensi' id='Kompetensi' value='{{ $row['Kompetensi'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
