@extends('layouts.app')

@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>
<div class="card">
	<div class="card-body">

		<div class="toolbar-nav">
			<div class="row">
				<div class="col-md-6 ">
					<div class="btn-group">
						<a href="{{ url('pendidik?return='.$return) }}" class="tips btn btn-danger  btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
						@if($access['is_add'] ==1)
				   		<a href="{{ url('pendidik/'.$id.'/edit?return='.$return) }}" class="tips btn btn-info btn-sm  " title="{{ __('core.btn_edit') }}"><i class="icon-note"></i></a>
						@endif
					</div>	
				</div>
				<div class="col-md-6 text-right">			
					<div class="btn-group">
				   		<a href="{{ ($prevnext['prev'] != '' ? url('pendidik/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-primary  btn-sm"><i class="fa fa-arrow-left"></i>  </a>	
						<a href="{{ ($prevnext['next'] != '' ? url('pendidik/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-primary btn-sm "> <i class="fa fa-arrow-right"></i>  </a>			
					</div>			
				</div>	

				
				
			</div>
		</div>
	
		<div class="table-responsive">
			<table class="table  table-bordered " >
				<tbody>	
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama', (isset($fields['Nama']['language'])? $fields['Nama']['language'] : array())) }}</td>
						<td>{{ $row->Nama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NUPTK', (isset($fields['NUPTK']['language'])? $fields['NUPTK']['language'] : array())) }}</td>
						<td>{{ $row->NUPTK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JK', (isset($fields['JK']['language'])? $fields['JK']['language'] : array())) }}</td>
						<td>{{ $row->JK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tempat Lahir', (isset($fields['Tempat_Lahir']['language'])? $fields['Tempat_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tempat_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tanggal Lahir', (isset($fields['Tanggal_Lahir']['language'])? $fields['Tanggal_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tanggal_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NIP', (isset($fields['NIP']['language'])? $fields['NIP']['language'] : array())) }}</td>
						<td>{{ $row->NIP}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status Kepegawaian', (isset($fields['Status_Kepegawaian']['language'])? $fields['Status_Kepegawaian']['language'] : array())) }}</td>
						<td>{{ $row->Status_Kepegawaian}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenis PTK', (isset($fields['Jenis_PTK']['language'])? $fields['Jenis_PTK']['language'] : array())) }}</td>
						<td>{{ $row->Jenis_PTK}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gelar Depan', (isset($fields['Gelar_Depan']['language'])? $fields['Gelar_Depan']['language'] : array())) }}</td>
						<td>{{ $row->Gelar_Depan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gelar Belakang', (isset($fields['Gelar_Belakang']['language'])? $fields['Gelar_Belakang']['language'] : array())) }}</td>
						<td>{{ $row->Gelar_Belakang}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenjang', (isset($fields['Jenjang']['language'])? $fields['Jenjang']['language'] : array())) }}</td>
						<td>{{ $row->Jenjang}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jurusan Prodi', (isset($fields['Jurusan_Prodi']['language'])? $fields['Jurusan_Prodi']['language'] : array())) }}</td>
						<td>{{ $row->Jurusan_Prodi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sertifikasi', (isset($fields['Sertifikasi']['language'])? $fields['Sertifikasi']['language'] : array())) }}</td>
						<td>{{ $row->Sertifikasi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('TMT Kerja', (isset($fields['TMT_Kerja']['language'])? $fields['TMT_Kerja']['language'] : array())) }}</td>
						<td>{{ $row->TMT_Kerja}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tugas Tambahan', (isset($fields['Tugas_Tambahan']['language'])? $fields['Tugas_Tambahan']['language'] : array())) }}</td>
						<td>{{ $row->Tugas_Tambahan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Mengajar', (isset($fields['Mengajar']['language'])? $fields['Mengajar']['language'] : array())) }}</td>
						<td>{{ $row->Mengajar}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jam Tugas Tambahan', (isset($fields['Jam_Tugas_Tambahan']['language'])? $fields['Jam_Tugas_Tambahan']['language'] : array())) }}</td>
						<td>{{ $row->Jam_Tugas_Tambahan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JJM', (isset($fields['JJM']['language'])? $fields['JJM']['language'] : array())) }}</td>
						<td>{{ $row->JJM}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Total JJM', (isset($fields['Total_JJM']['language'])? $fields['Total_JJM']['language'] : array())) }}</td>
						<td>{{ $row->Total_JJM}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Siswa', (isset($fields['Siswa']['language'])? $fields['Siswa']['language'] : array())) }}</td>
						<td>{{ $row->Siswa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kompetensi', (isset($fields['Kompetensi']['language'])? $fields['Kompetensi']['language'] : array())) }}</td>
						<td>{{ $row->Kompetensi}} </td>
						
					</tr>
				
				</tbody>	
			</table>   

		 	

		</div>
			
	</div>
</div>		
@stop
