<!-- <section id="intro" class="">

  <div class="intro-content">



  </div>
</section> -->

<br></br>
<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script language="javascript">
  'use strict';
  $(function () {
    var width = 720;
    var animationSpeed = 2500;
    var pause = 5000;
    var currentSlide = 1;
    var $sdr0934 = $('#sdr0934');
    var $slideContainer = $('.sld', $sdr0934);
    var $sld = $('.slide', $sdr0934);
    var interval;

    function startsdr0934() {
      interval = setInterval(function () {
        $slideContainer.animate({
          'margin-left': '-=' + width
        }, animationSpeed, function () {
          if (++currentSlide === $sld.length) {
            currentSlide = 1;
            $slideContainer.css('margin-left', 0);
          }
        });
      }, pause);
    }

    function pausesdr0934() {
      clearInterval(interval);
    }
    $slideContainer.on('mouseenter', pausesdr0934).on('mouseleave', startsdr0934);
    startsdr0934();
  });
</script>
<div style="width: 720px; margin: 0 auto;">
  <div style="position: relative; width: 720px; ">
    <style type="text/css">
      #gpoy {
        position: absolute;
        right: -168px;
        top: 3px;
        padding: 8px 0px 3px;
        z-index: 99;
        opacity: 0.9;
        transition: 1s;
        line-height: 12px;
      }

      .repper {
        width: 726px;
        margin: 0 auto;
        position: relative;
        overflow: hidden;
        border-radius: 20px;
        box-shadow: 7px 7px 9px #888888;
      }

      .repper:hover #gpoy {
        transition: 0.5s;
        right: 23px;
      }
    </style>
    <div class="repper">

      <style type="text/css">
        #sdr0934 {
          width: 720px;
          height: 400px;
          border: 3px solid #FFFFFF;
          border-radius: 20px;
          overflow: hidden;
          position: relative;
          cursor: pointer;
        }

        #sdr0934 .sld {
          width: 5040px;
          height: 400px;
          display: block;
          margin: 0;
          padding: 0;
        }

        #sdr0934 .slide {
          width: 720px;
          height: 400px;
          float: left;
          list-style-type: none;
        }

        .MIW220406 {
          position: absolute;
          max-width: 720px;
          margin: 0px;
        }

        .c181695 {
          position: absolute;
          bottom: 0px;
          width: 100%;
          padding: 12px;
          background-color: rgba(0, 0, 0, 0.5);
          color: #FFFFFF;
          font-size: 24pt;
          font-family: Tahoma;
          text-align: center;
          line-height: 48px;
          z-index: 33;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
        }
      </style>

      <div id="sdr0934">
        <ul class="sld">

          <li class="slide slide1">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-1.jpg">
            </figure>
          </li>

          <li class="slide slide2">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-2.jpg">
            </figure>
          </li>

          <li class="slide slide3">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-3.jpg">
            </figure>
          </li>

          <li class="slide slide4">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-4.jpg">
            </figure>
          </li>

          <li class="slide slide5">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-5.jpg">
            </figure>
          </li>

          <li class="slide slide6">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-6.jpg">
            </figure>
          </li>

          <li class="slide slide1">
            <figure class="MIW220406">
              <IMG SRC="uploads/images/home/181695-1.jpg">
            </figure>
          </li>

        </ul>
      </div>
    </div>
  </div>
</div>

<section class="block-section bg-grey">
  <div class="container">
    <h3> <b> UPTD SATDIK SDN Mumbulsari 01 </b> </h3>
    <p> Jalan Budi Utomo No.56, Mumbulsari, Jember, Jawa Timur</p>
    <a href="{{ url('user/login') }}"><button type="button" class="btn btn-primary btn-sm">Masuk</button></a>
    <a href="{{ url('posts/') }}"><button type="button" class="btn btn-warning btn-sm">Pengumuman</button></a>
    <!-- <a  href="{{ url('kelulusan/') }}"><button type="button" class="btn btn-link btn-sm"> <b>Lihat Status Kelulusan</b> </button></a>    -->
    <br></br>
    <form action="" method="GET">
      <label for="cekkelulusan"> Cek Kelulusan </label>
      <input id="cekkelulusan" name="cekkelulusan" type="number" style="width: auto; height: auto;" class="form-input"
        placeholder="Masukan NISN/NIPD" required>
      <button style="background-color: white;" type="submit"><b> Cari</b></button>
    </form>

    <br></br>



    <!-- table kelulusan -->
    

    @foreach ($kelulusan as $var)
    <table class="table" {{$vtable}}>
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama</th>
          <th scope="col">NISN</th>
          <th scope="col">NIPD</th>
          <th scope="col">Status Kelulusan</th>
          <th scope="col">SKHUN</th>
          <th scope="col">Ijasah</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <th scope="row">1</th>
          <td> <b> {{$var->nama}} </b></td>
          <td> <b> {{$var->nisn}} </b></td>
          <td> <b> {{$var->nipd}} </b></td>
          <td> <b>
          @php
            if($var->id_statuskelulusan == "1"){
              echo "LULUS";
            }else{
              echo "TIDAK LULUS";
            }
          @endphp  
           

          </b></td>
          <td> <a href="/uploads/images/skhun/{{$var->skhun}}" download><b>Unduh</b></a> </td>
          <td> <a href="/uploads/images/ijasah/{{$var->ijasah}}" download><b>Unduh</b></a> </td>
        </tr>

      </tbody>

      @endforeach
    </table>

  </div>
</section>