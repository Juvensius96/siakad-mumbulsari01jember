

		 {!! Form::open(array('url'=>'raport', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Raport</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Nama" class=" control-label col-md-4 "> Nama </label>
										<div class="col-md-8">
										  <input  type='text' name='nama' id='nama' value='{{ $row['nama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nipd" class=" control-label col-md-4 "> Nipd </label>
										<div class="col-md-8">
										  <input  type='text' name='nipd' id='nipd' value='{{ $row['nipd'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kelas" class=" control-label col-md-4 "> Kelas </label>
										<div class="col-md-8">
										  <input  type='text' name='kelas' id='kelas' value='{{ $row['kelas'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Semester" class=" control-label col-md-4 "> Semester </label>
										<div class="col-md-8">
										  <input  type='text' name='semester' id='semester' value='{{ $row['semester'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tahun Ajaran" class=" control-label col-md-4 "> Tahun Ajaran </label>
										<div class="col-md-8">
										  <input  type='text' name='tahun_ajaran' id='tahun_ajaran' value='{{ $row['tahun_ajaran'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sikap Spiritual" class=" control-label col-md-4 "> Sikap Spiritual </label>
										<div class="col-md-8">
										  <input  type='text' name='sikap_spiritual' id='sikap_spiritual' value='{{ $row['sikap_spiritual'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sikap Sosial" class=" control-label col-md-4 "> Sikap Sosial </label>
										<div class="col-md-8">
										  <input  type='text' name='sikap_sosial' id='sikap_sosial' value='{{ $row['sikap_sosial'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Ekstrakurikuler" class=" control-label col-md-4 "> Nama Ekstrakurikuler </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_ekstrakurikuler' id='nama_ekstrakurikuler' value='{{ $row['nama_ekstrakurikuler'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Keterangan Ekstrakurikuler" class=" control-label col-md-4 "> Keterangan Ekstrakurikuler </label>
										<div class="col-md-8">
										  <input  type='text' name='keterangan_ekstrakurikuler' id='keterangan_ekstrakurikuler' value='{{ $row['keterangan_ekstrakurikuler'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Saran" class=" control-label col-md-4 "> Saran </label>
										<div class="col-md-8">
										  <input  type='text' name='saran' id='saran' value='{{ $row['saran'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tinggibadan S1" class=" control-label col-md-4 "> Tinggibadan S1 </label>
										<div class="col-md-8">
										  <input  type='text' name='tinggibadan_s1' id='tinggibadan_s1' value='{{ $row['tinggibadan_s1'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tinggibadan S2" class=" control-label col-md-4 "> Tinggibadan S2 </label>
										<div class="col-md-8">
										  <input  type='text' name='tinggibadan_s2' id='tinggibadan_s2' value='{{ $row['tinggibadan_s2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Beratbadan S1" class=" control-label col-md-4 "> Beratbadan S1 </label>
										<div class="col-md-8">
										  <input  type='text' name='beratbadan_s1' id='beratbadan_s1' value='{{ $row['beratbadan_s1'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Beratbadan S2" class=" control-label col-md-4 "> Beratbadan S2 </label>
										<div class="col-md-8">
										  <input  type='text' name='beratbadan_s2' id='beratbadan_s2' value='{{ $row['beratbadan_s2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kondisi Pendengaran" class=" control-label col-md-4 "> Kondisi Pendengaran </label>
										<div class="col-md-8">
										  <input  type='text' name='kondisi_pendengaran' id='kondisi_pendengaran' value='{{ $row['kondisi_pendengaran'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kondisi Penglihatan" class=" control-label col-md-4 "> Kondisi Penglihatan </label>
										<div class="col-md-8">
										  <input  type='text' name='kondisi_penglihatan' id='kondisi_penglihatan' value='{{ $row['kondisi_penglihatan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kondisi Gigi" class=" control-label col-md-4 "> Kondisi Gigi </label>
										<div class="col-md-8">
										  <input  type='text' name='kondisi_gigi' id='kondisi_gigi' value='{{ $row['kondisi_gigi'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jenisprestasi S1" class=" control-label col-md-4 "> Jenisprestasi S1 </label>
										<div class="col-md-8">
										  <input  type='text' name='jenisprestasi_s1' id='jenisprestasi_s1' value='{{ $row['jenisprestasi_s1'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Prestasi S1" class=" control-label col-md-4 "> Prestasi S1 </label>
										<div class="col-md-8">
										  <input  type='text' name='prestasi_s1' id='prestasi_s1' value='{{ $row['prestasi_s1'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jenisprestasi S2" class=" control-label col-md-4 "> Jenisprestasi S2 </label>
										<div class="col-md-8">
										  <input  type='text' name='jenisprestasi_s2' id='jenisprestasi_s2' value='{{ $row['jenisprestasi_s2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Prestasi S2" class=" control-label col-md-4 "> Prestasi S2 </label>
										<div class="col-md-8">
										  <input  type='text' name='prestasi_s2' id='prestasi_s2' value='{{ $row['prestasi_s2'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jumlah Sakit" class=" control-label col-md-4 "> Jumlah Sakit </label>
										<div class="col-md-8">
										  <input  type='text' name='jumlah_sakit' id='jumlah_sakit' value='{{ $row['jumlah_sakit'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jumlah Izin" class=" control-label col-md-4 "> Jumlah Izin </label>
										<div class="col-md-8">
										  <input  type='text' name='jumlah_izin' id='jumlah_izin' value='{{ $row['jumlah_izin'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jumlah Alpha" class=" control-label col-md-4 "> Jumlah Alpha </label>
										<div class="col-md-8">
										  <input  type='text' name='jumlah_alpha' id='jumlah_alpha' value='{{ $row['jumlah_alpha'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Agama" class=" control-label col-md-4 "> Nilai Pengetahuan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_agama' id='nilai_pengetahuan_agama' value='{{ $row['nilai_pengetahuan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Agama" class=" control-label col-md-4 "> Predikat Pengetahuan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_agama' id='predikat_pengetahuan_agama' value='{{ $row['predikat_pengetahuan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Agama" class=" control-label col-md-4 "> Deskripsi Pengetahuan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_agama' id='deskripsi_pengetahuan_agama' value='{{ $row['deskripsi_pengetahuan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Agama" class=" control-label col-md-4 "> Nilai Ketrampilan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_agama' id='nilai_ketrampilan_agama' value='{{ $row['nilai_ketrampilan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Agama" class=" control-label col-md-4 "> Predikat Ketrampilan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_agama' id='predikat_ketrampilan_agama' value='{{ $row['predikat_ketrampilan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Agama" class=" control-label col-md-4 "> Deskripsi Ketrampilan Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_agama' id='deskripsi_ketrampilan_agama' value='{{ $row['deskripsi_ketrampilan_agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Pancasila" class=" control-label col-md-4 "> Nilai Pengetahuan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_pancasila' id='nilai_pengetahuan_pancasila' value='{{ $row['nilai_pengetahuan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Pancasila" class=" control-label col-md-4 "> Predikat Pengetahuan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_pancasila' id='predikat_pengetahuan_pancasila' value='{{ $row['predikat_pengetahuan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Pancasila" class=" control-label col-md-4 "> Deskripsi Pengetahuan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_pancasila' id='deskripsi_pengetahuan_pancasila' value='{{ $row['deskripsi_pengetahuan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Pancasila" class=" control-label col-md-4 "> Nilai Ketrampilan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_pancasila' id='nilai_ketrampilan_pancasila' value='{{ $row['nilai_ketrampilan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Pancasila" class=" control-label col-md-4 "> Predikat Ketrampilan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_pancasila' id='predikat_ketrampilan_pancasila' value='{{ $row['predikat_ketrampilan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Pancasila" class=" control-label col-md-4 "> Deskripsi Ketrampilan Pancasila </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_pancasila' id='deskripsi_ketrampilan_pancasila' value='{{ $row['deskripsi_ketrampilan_pancasila'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Bahasaindo" class=" control-label col-md-4 "> Nilai Pengetahuan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_bahasaindo' id='nilai_pengetahuan_bahasaindo' value='{{ $row['nilai_pengetahuan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Bahasaindo" class=" control-label col-md-4 "> Predikat Pengetahuan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_bahasaindo' id='predikat_pengetahuan_bahasaindo' value='{{ $row['predikat_pengetahuan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Bahasaindo" class=" control-label col-md-4 "> Deskripsi Pengetahuan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_bahasaindo' id='deskripsi_pengetahuan_bahasaindo' value='{{ $row['deskripsi_pengetahuan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Bahasaindo" class=" control-label col-md-4 "> Nilai Ketrampilan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_bahasaindo' id='nilai_ketrampilan_bahasaindo' value='{{ $row['nilai_ketrampilan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Bahasaindo" class=" control-label col-md-4 "> Predikat Ketrampilan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_bahasaindo' id='predikat_ketrampilan_bahasaindo' value='{{ $row['predikat_ketrampilan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Bahasaindo" class=" control-label col-md-4 "> Deskripsi Ketrampilan Bahasaindo </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_bahasaindo' id='deskripsi_ketrampilan_bahasaindo' value='{{ $row['deskripsi_ketrampilan_bahasaindo'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Matematika" class=" control-label col-md-4 "> Nilai Pengetahuan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_matematika' id='nilai_pengetahuan_matematika' value='{{ $row['nilai_pengetahuan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Matematika" class=" control-label col-md-4 "> Predikat Pengetahuan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_matematika' id='predikat_pengetahuan_matematika' value='{{ $row['predikat_pengetahuan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Matematika" class=" control-label col-md-4 "> Deskripsi Pengetahuan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_matematika' id='deskripsi_pengetahuan_matematika' value='{{ $row['deskripsi_pengetahuan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Matematika" class=" control-label col-md-4 "> Nilai Ketrampilan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_matematika' id='nilai_ketrampilan_matematika' value='{{ $row['nilai_ketrampilan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Matematika" class=" control-label col-md-4 "> Predikat Ketrampilan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_matematika' id='predikat_ketrampilan_matematika' value='{{ $row['predikat_ketrampilan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Matematika" class=" control-label col-md-4 "> Deskripsi Ketrampilan Matematika </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_matematika' id='deskripsi_ketrampilan_matematika' value='{{ $row['deskripsi_ketrampilan_matematika'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Ipa" class=" control-label col-md-4 "> Nilai Pengetahuan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_ipa' id='nilai_pengetahuan_ipa' value='{{ $row['nilai_pengetahuan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Ipa" class=" control-label col-md-4 "> Predikat Pengetahuan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_ipa' id='predikat_pengetahuan_ipa' value='{{ $row['predikat_pengetahuan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Ipa" class=" control-label col-md-4 "> Deskripsi Pengetahuan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_ipa' id='deskripsi_pengetahuan_ipa' value='{{ $row['deskripsi_pengetahuan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Ipa" class=" control-label col-md-4 "> Nilai Ketrampilan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_ipa' id='nilai_ketrampilan_ipa' value='{{ $row['nilai_ketrampilan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Ipa" class=" control-label col-md-4 "> Predikat Ketrampilan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_ipa' id='predikat_ketrampilan_ipa' value='{{ $row['predikat_ketrampilan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Ipa" class=" control-label col-md-4 "> Deskripsi Ketrampilan Ipa </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_ipa' id='deskripsi_ketrampilan_ipa' value='{{ $row['deskripsi_ketrampilan_ipa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Ips" class=" control-label col-md-4 "> Nilai Pengetahuan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_ips' id='nilai_pengetahuan_ips' value='{{ $row['nilai_pengetahuan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Ips" class=" control-label col-md-4 "> Predikat Pengetahuan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_ips' id='predikat_pengetahuan_ips' value='{{ $row['predikat_pengetahuan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Ips" class=" control-label col-md-4 "> Deskripsi Pengetahuan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_ips' id='deskripsi_pengetahuan_ips' value='{{ $row['deskripsi_pengetahuan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Ips" class=" control-label col-md-4 "> Nilai Ketrampilan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_ips' id='nilai_ketrampilan_ips' value='{{ $row['nilai_ketrampilan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Ips" class=" control-label col-md-4 "> Predikat Ketrampilan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_ips' id='predikat_ketrampilan_ips' value='{{ $row['predikat_ketrampilan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Ips" class=" control-label col-md-4 "> Deskripsi Ketrampilan Ips </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_ips' id='deskripsi_ketrampilan_ips' value='{{ $row['deskripsi_ketrampilan_ips'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Budaya" class=" control-label col-md-4 "> Nilai Pengetahuan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_budaya' id='nilai_pengetahuan_budaya' value='{{ $row['nilai_pengetahuan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Budaya" class=" control-label col-md-4 "> Predikat Pengetahuan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_budaya' id='predikat_pengetahuan_budaya' value='{{ $row['predikat_pengetahuan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Budaya" class=" control-label col-md-4 "> Deskripsi Pengetahuan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_budaya' id='deskripsi_pengetahuan_budaya' value='{{ $row['deskripsi_pengetahuan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Budaya" class=" control-label col-md-4 "> Nilai Ketrampilan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_budaya' id='nilai_ketrampilan_budaya' value='{{ $row['nilai_ketrampilan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Budaya" class=" control-label col-md-4 "> Predikat Ketrampilan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_budaya' id='predikat_ketrampilan_budaya' value='{{ $row['predikat_ketrampilan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Budaya" class=" control-label col-md-4 "> Deskripsi Ketrampilan Budaya </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_budaya' id='deskripsi_ketrampilan_budaya' value='{{ $row['deskripsi_ketrampilan_budaya'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Penjasorkes" class=" control-label col-md-4 "> Nilai Pengetahuan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_penjasorkes' id='nilai_pengetahuan_penjasorkes' value='{{ $row['nilai_pengetahuan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Penjasorkes" class=" control-label col-md-4 "> Predikat Pengetahuan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_penjasorkes' id='predikat_pengetahuan_penjasorkes' value='{{ $row['predikat_pengetahuan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Penjasorkes" class=" control-label col-md-4 "> Deskripsi Pengetahuan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_penjasorkes' id='deskripsi_pengetahuan_penjasorkes' value='{{ $row['deskripsi_pengetahuan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Penjasorkes" class=" control-label col-md-4 "> Nilai Ketrampilan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_penjasorkes' id='nilai_ketrampilan_penjasorkes' value='{{ $row['nilai_ketrampilan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Penjasorkes" class=" control-label col-md-4 "> Predikat Ketrampilan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_penjasorkes' id='predikat_ketrampilan_penjasorkes' value='{{ $row['predikat_ketrampilan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Penjasorkes" class=" control-label col-md-4 "> Deskripsi Ketrampilan Penjasorkes </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_penjasorkes' id='deskripsi_ketrampilan_penjasorkes' value='{{ $row['deskripsi_ketrampilan_penjasorkes'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Pengetahuan Bahasadaerah" class=" control-label col-md-4 "> Nilai Pengetahuan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_pengetahuan_bahasadaerah' id='nilai_pengetahuan_bahasadaerah' value='{{ $row['nilai_pengetahuan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Pengetahuan Bahasadaerah" class=" control-label col-md-4 "> Predikat Pengetahuan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_pengetahuan_bahasadaerah' id='predikat_pengetahuan_bahasadaerah' value='{{ $row['predikat_pengetahuan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Pengetahuan Bahasadaerah" class=" control-label col-md-4 "> Deskripsi Pengetahuan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_pengetahuan_bahasadaerah' id='deskripsi_pengetahuan_bahasadaerah' value='{{ $row['deskripsi_pengetahuan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nilai Ketrampilan Bahasadaerah" class=" control-label col-md-4 "> Nilai Ketrampilan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='nilai_ketrampilan_bahasadaerah' id='nilai_ketrampilan_bahasadaerah' value='{{ $row['nilai_ketrampilan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Predikat Ketrampilan Bahasadaerah" class=" control-label col-md-4 "> Predikat Ketrampilan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='predikat_ketrampilan_bahasadaerah' id='predikat_ketrampilan_bahasadaerah' value='{{ $row['predikat_ketrampilan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Deskripsi Ketrampilan Bahasadaerah" class=" control-label col-md-4 "> Deskripsi Ketrampilan Bahasadaerah </label>
										<div class="col-md-8">
										  <input  type='text' name='deskripsi_ketrampilan_bahasadaerah' id='deskripsi_ketrampilan_bahasadaerah' value='{{ $row['deskripsi_ketrampilan_bahasadaerah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
