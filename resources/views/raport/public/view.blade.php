<div class="container" class="pt-3 pb-3">
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-container" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama', (isset($fields['nama']['language'])? $fields['nama']['language'] : array())) }}</td>
						<td>{{ $row->nama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nipd', (isset($fields['nipd']['language'])? $fields['nipd']['language'] : array())) }}</td>
						<td>{{ $row->nipd}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kelas', (isset($fields['kelas']['language'])? $fields['kelas']['language'] : array())) }}</td>
						<td>{{ $row->kelas}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Semester', (isset($fields['semester']['language'])? $fields['semester']['language'] : array())) }}</td>
						<td>{{ $row->semester}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tahun Ajaran', (isset($fields['tahun_ajaran']['language'])? $fields['tahun_ajaran']['language'] : array())) }}</td>
						<td>{{ $row->tahun_ajaran}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sikap Spiritual', (isset($fields['sikap_spiritual']['language'])? $fields['sikap_spiritual']['language'] : array())) }}</td>
						<td>{{ $row->sikap_spiritual}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sikap Sosial', (isset($fields['sikap_sosial']['language'])? $fields['sikap_sosial']['language'] : array())) }}</td>
						<td>{{ $row->sikap_sosial}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Ekstrakurikuler', (isset($fields['nama_ekstrakurikuler']['language'])? $fields['nama_ekstrakurikuler']['language'] : array())) }}</td>
						<td>{{ $row->nama_ekstrakurikuler}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Keterangan Ekstrakurikuler', (isset($fields['keterangan_ekstrakurikuler']['language'])? $fields['keterangan_ekstrakurikuler']['language'] : array())) }}</td>
						<td>{{ $row->keterangan_ekstrakurikuler}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Saran', (isset($fields['saran']['language'])? $fields['saran']['language'] : array())) }}</td>
						<td>{{ $row->saran}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tinggibadan S1', (isset($fields['tinggibadan_s1']['language'])? $fields['tinggibadan_s1']['language'] : array())) }}</td>
						<td>{{ $row->tinggibadan_s1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tinggibadan S2', (isset($fields['tinggibadan_s2']['language'])? $fields['tinggibadan_s2']['language'] : array())) }}</td>
						<td>{{ $row->tinggibadan_s2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Beratbadan S1', (isset($fields['beratbadan_s1']['language'])? $fields['beratbadan_s1']['language'] : array())) }}</td>
						<td>{{ $row->beratbadan_s1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Beratbadan S2', (isset($fields['beratbadan_s2']['language'])? $fields['beratbadan_s2']['language'] : array())) }}</td>
						<td>{{ $row->beratbadan_s2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kondisi Pendengaran', (isset($fields['kondisi_pendengaran']['language'])? $fields['kondisi_pendengaran']['language'] : array())) }}</td>
						<td>{{ $row->kondisi_pendengaran}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kondisi Penglihatan', (isset($fields['kondisi_penglihatan']['language'])? $fields['kondisi_penglihatan']['language'] : array())) }}</td>
						<td>{{ $row->kondisi_penglihatan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kondisi Gigi', (isset($fields['kondisi_gigi']['language'])? $fields['kondisi_gigi']['language'] : array())) }}</td>
						<td>{{ $row->kondisi_gigi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenisprestasi S1', (isset($fields['jenisprestasi_s1']['language'])? $fields['jenisprestasi_s1']['language'] : array())) }}</td>
						<td>{{ $row->jenisprestasi_s1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Prestasi S1', (isset($fields['prestasi_s1']['language'])? $fields['prestasi_s1']['language'] : array())) }}</td>
						<td>{{ $row->prestasi_s1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenisprestasi S2', (isset($fields['jenisprestasi_s2']['language'])? $fields['jenisprestasi_s2']['language'] : array())) }}</td>
						<td>{{ $row->jenisprestasi_s2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Prestasi S2', (isset($fields['prestasi_s2']['language'])? $fields['prestasi_s2']['language'] : array())) }}</td>
						<td>{{ $row->prestasi_s2}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jumlah Sakit', (isset($fields['jumlah_sakit']['language'])? $fields['jumlah_sakit']['language'] : array())) }}</td>
						<td>{{ $row->jumlah_sakit}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jumlah Izin', (isset($fields['jumlah_izin']['language'])? $fields['jumlah_izin']['language'] : array())) }}</td>
						<td>{{ $row->jumlah_izin}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jumlah Alpha', (isset($fields['jumlah_alpha']['language'])? $fields['jumlah_alpha']['language'] : array())) }}</td>
						<td>{{ $row->jumlah_alpha}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Agama', (isset($fields['nilai_pengetahuan_agama']['language'])? $fields['nilai_pengetahuan_agama']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Agama', (isset($fields['predikat_pengetahuan_agama']['language'])? $fields['predikat_pengetahuan_agama']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Agama', (isset($fields['deskripsi_pengetahuan_agama']['language'])? $fields['deskripsi_pengetahuan_agama']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Agama', (isset($fields['nilai_ketrampilan_agama']['language'])? $fields['nilai_ketrampilan_agama']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Agama', (isset($fields['predikat_ketrampilan_agama']['language'])? $fields['predikat_ketrampilan_agama']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Agama', (isset($fields['deskripsi_ketrampilan_agama']['language'])? $fields['deskripsi_ketrampilan_agama']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Pancasila', (isset($fields['nilai_pengetahuan_pancasila']['language'])? $fields['nilai_pengetahuan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Pancasila', (isset($fields['predikat_pengetahuan_pancasila']['language'])? $fields['predikat_pengetahuan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Pancasila', (isset($fields['deskripsi_pengetahuan_pancasila']['language'])? $fields['deskripsi_pengetahuan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Pancasila', (isset($fields['nilai_ketrampilan_pancasila']['language'])? $fields['nilai_ketrampilan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Pancasila', (isset($fields['predikat_ketrampilan_pancasila']['language'])? $fields['predikat_ketrampilan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Pancasila', (isset($fields['deskripsi_ketrampilan_pancasila']['language'])? $fields['deskripsi_ketrampilan_pancasila']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_pancasila}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Bahasaindo', (isset($fields['nilai_pengetahuan_bahasaindo']['language'])? $fields['nilai_pengetahuan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Bahasaindo', (isset($fields['predikat_pengetahuan_bahasaindo']['language'])? $fields['predikat_pengetahuan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Bahasaindo', (isset($fields['deskripsi_pengetahuan_bahasaindo']['language'])? $fields['deskripsi_pengetahuan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Bahasaindo', (isset($fields['nilai_ketrampilan_bahasaindo']['language'])? $fields['nilai_ketrampilan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Bahasaindo', (isset($fields['predikat_ketrampilan_bahasaindo']['language'])? $fields['predikat_ketrampilan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Bahasaindo', (isset($fields['deskripsi_ketrampilan_bahasaindo']['language'])? $fields['deskripsi_ketrampilan_bahasaindo']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_bahasaindo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Matematika', (isset($fields['nilai_pengetahuan_matematika']['language'])? $fields['nilai_pengetahuan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Matematika', (isset($fields['predikat_pengetahuan_matematika']['language'])? $fields['predikat_pengetahuan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Matematika', (isset($fields['deskripsi_pengetahuan_matematika']['language'])? $fields['deskripsi_pengetahuan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Matematika', (isset($fields['nilai_ketrampilan_matematika']['language'])? $fields['nilai_ketrampilan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Matematika', (isset($fields['predikat_ketrampilan_matematika']['language'])? $fields['predikat_ketrampilan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Matematika', (isset($fields['deskripsi_ketrampilan_matematika']['language'])? $fields['deskripsi_ketrampilan_matematika']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_matematika}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Ipa', (isset($fields['nilai_pengetahuan_ipa']['language'])? $fields['nilai_pengetahuan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Ipa', (isset($fields['predikat_pengetahuan_ipa']['language'])? $fields['predikat_pengetahuan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Ipa', (isset($fields['deskripsi_pengetahuan_ipa']['language'])? $fields['deskripsi_pengetahuan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Ipa', (isset($fields['nilai_ketrampilan_ipa']['language'])? $fields['nilai_ketrampilan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Ipa', (isset($fields['predikat_ketrampilan_ipa']['language'])? $fields['predikat_ketrampilan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Ipa', (isset($fields['deskripsi_ketrampilan_ipa']['language'])? $fields['deskripsi_ketrampilan_ipa']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_ipa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Ips', (isset($fields['nilai_pengetahuan_ips']['language'])? $fields['nilai_pengetahuan_ips']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Ips', (isset($fields['predikat_pengetahuan_ips']['language'])? $fields['predikat_pengetahuan_ips']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Ips', (isset($fields['deskripsi_pengetahuan_ips']['language'])? $fields['deskripsi_pengetahuan_ips']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Ips', (isset($fields['nilai_ketrampilan_ips']['language'])? $fields['nilai_ketrampilan_ips']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Ips', (isset($fields['predikat_ketrampilan_ips']['language'])? $fields['predikat_ketrampilan_ips']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Ips', (isset($fields['deskripsi_ketrampilan_ips']['language'])? $fields['deskripsi_ketrampilan_ips']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_ips}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Budaya', (isset($fields['nilai_pengetahuan_budaya']['language'])? $fields['nilai_pengetahuan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Budaya', (isset($fields['predikat_pengetahuan_budaya']['language'])? $fields['predikat_pengetahuan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Budaya', (isset($fields['deskripsi_pengetahuan_budaya']['language'])? $fields['deskripsi_pengetahuan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Budaya', (isset($fields['nilai_ketrampilan_budaya']['language'])? $fields['nilai_ketrampilan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Budaya', (isset($fields['predikat_ketrampilan_budaya']['language'])? $fields['predikat_ketrampilan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Budaya', (isset($fields['deskripsi_ketrampilan_budaya']['language'])? $fields['deskripsi_ketrampilan_budaya']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_budaya}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Penjasorkes', (isset($fields['nilai_pengetahuan_penjasorkes']['language'])? $fields['nilai_pengetahuan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Penjasorkes', (isset($fields['predikat_pengetahuan_penjasorkes']['language'])? $fields['predikat_pengetahuan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Penjasorkes', (isset($fields['deskripsi_pengetahuan_penjasorkes']['language'])? $fields['deskripsi_pengetahuan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Penjasorkes', (isset($fields['nilai_ketrampilan_penjasorkes']['language'])? $fields['nilai_ketrampilan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Penjasorkes', (isset($fields['predikat_ketrampilan_penjasorkes']['language'])? $fields['predikat_ketrampilan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Penjasorkes', (isset($fields['deskripsi_ketrampilan_penjasorkes']['language'])? $fields['deskripsi_ketrampilan_penjasorkes']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_penjasorkes}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Pengetahuan Bahasadaerah', (isset($fields['nilai_pengetahuan_bahasadaerah']['language'])? $fields['nilai_pengetahuan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->nilai_pengetahuan_bahasadaerah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Pengetahuan Bahasadaerah', (isset($fields['predikat_pengetahuan_bahasadaerah']['language'])? $fields['predikat_pengetahuan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->predikat_pengetahuan_bahasadaerah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Pengetahuan Bahasadaerah', (isset($fields['deskripsi_pengetahuan_bahasadaerah']['language'])? $fields['deskripsi_pengetahuan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_pengetahuan_bahasadaerah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nilai Ketrampilan Bahasadaerah', (isset($fields['nilai_ketrampilan_bahasadaerah']['language'])? $fields['nilai_ketrampilan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->nilai_ketrampilan_bahasadaerah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Predikat Ketrampilan Bahasadaerah', (isset($fields['predikat_ketrampilan_bahasadaerah']['language'])? $fields['predikat_ketrampilan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->predikat_ketrampilan_bahasadaerah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Deskripsi Ketrampilan Bahasadaerah', (isset($fields['deskripsi_ketrampilan_bahasadaerah']['language'])? $fields['deskripsi_ketrampilan_bahasadaerah']['language'] : array())) }}</td>
						<td>{{ $row->deskripsi_ketrampilan_bahasadaerah}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	