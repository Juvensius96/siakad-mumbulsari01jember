@extends('layouts.app')

@section('content')
<div class="page-titles">
	<h2> {{ $pageTitle }} </h2>
</div>
<div class="card">
	<div class="card-body">

		<div class="toolbar-nav">
			<div class="row">
				<div class="col-md-6 ">
					<div class="btn-group">
						<a href="{{ url('raport?return='.$return) }}" class="tips btn btn-danger  btn-sm  "
							title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>
						@if($access['is_add'] ==1)
						<a href="{{ url('raport/'.$id.'/edit?return='.$return) }}" class="tips btn btn-info btn-sm  "
							title="{{ __('core.btn_edit') }}"><i class="icon-note"></i></a>
						@endif
					</div>
				</div>
				<div class="col-md-6 text-right">
					<div class="btn-group">

						<button class="btn btn-success btn-sm" name="cetakraport" id="cetakraport"><i
								class="fa fa-print"></i> <a
								href="{{ url( $pageModule .'/cetakraport?do=cetakexcel&return='.$return) }}">Cetak</a> </button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<body>
	<div class="card">
	<div class="card-body">

		<table class="table table-bordered" id="tabel1" name="tabel1">
			<h4 style="text-align:center ;"><b>RAPORT PESERTA DIDIK DAN PROFILE PESERTA DIDIK</b></h4>
			<tr></tr>
			<tr>
				<td>Nama Peserta
					Didik&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$row->nama}}
				</td>
				<td>Kelas&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$row->kelas_asal}}
				</td>
			</tr>
			<tr>
				<td>Nomor Induk
					Siswa&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$row->nipd}}
				</td>
				<td>Semester&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$row->semester}}
				</td>
			</tr>
			<tr>
				<td>Nama Sekolah&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;SDN
					Mumbulsari 01 Jember</td>
				<td>Tahun Pelajaran&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$row->tahun_ajaran}}</td>
			</tr>
			<tr>
				<td>Alamat Sekolah&nbsp;:&nbsp;</td>
				<td></td>
			</tr>

		</table>

		<br></br>

		<style>
			.tb-sikap {
				text-align: center;
			}
		</style>
		<table class="table table-bordered">
			<p><b>A.Sikap</b></p>
			<tr>
				<td colspan="3" style="text-align:center ;"><b>DESKRIPSI</b></td>
			</tr>
			<tr>
				<td class="tb-sikap">1</td>
				<td>Sikap Spiritual</td>
				<td>{{ $row->sikap_spiritual}} </td>
			</tr>
			<tr>
				<td class="tb-sikap">2</td>
				<td>Sikap Sosial</td>
				<td>{{ $row->sikap_sosial}} </td>
			</tr>
		</table>
		<table class="table table-bordered">
			<br></br>
			<p><b>Pengetahuan dan Ketrampilan</b></p>
			<p><b>KKM Satuan Pendidikan = 70</b></p>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<tr>
				<td rowspan="2">No</td>
				<td rowspan="2">Muatan Pelajaran</td>
				<td colspan="3">Pengetahuan</td>
				<td colspan="3">Ketrampilan</td>
			</tr>
			<tr>

				<td>Nilai</td>
				<td>Predikat</td>
				<td>Deskripsi</td>
				<td>Nilai</td>
				<td>Predikat</td>
				<td>Deskripsi</td>
			</tr>
			<tr>
				<td>1</td>
				<td>Pendidikan Agama dan Budi Pekerti</td>
				<td>{{$row->nilai_pengetahuan_agama}}</td>
				<td>{{$row->predikat_pengetahuan_agama}}</td>
				<td>{{$row->deskripsi_pengetahuan_agama}}</td>
				<td>{{$row->nilai_ketrampilan_agama}}</td>
				<td>{{$row->predikat_ketrampilan_agama}}</td>
				<td>{{$row->deskripsi_pengetahuan_agama}}</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Pendidikan Pancasila dan Kewarganegaraan</td>
				<td>{{$row->nilai_pengetahuan_pancasila}}</td>
				<td>{{$row->predikat_pengetahuan_pancasila}}</td>
				<td>{{$row->deskripsi_pengetahuan_pancasila}}</td>
				<td>{{$row->nilai_ketrampilan_pancasila}}</td>
				<td>{{$row->predikat_ketrampilan_pancasila}}</td>
				<td>{{$row->deskripsi_pengetahuan_pancasila}}</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Bahasa Indonesia</td>
				<td>{{$row->nilai_pengetahuan_bahasaindo}}</td>
				<td>{{$row->predikat_pengetahuan_bahasaindo}}</td>
				<td>{{$row->deskripsi_pengetahuan_bahasaindo}}</td>
				<td>{{$row->nilai_ketrampilan_bahasaindo}}</td>
				<td>{{$row->predikat_ketrampilan_bahasaindo}}</td>
				<td>{{$row->deskripsi_pengetahuan_bahasaindo}}</td>
			</tr>
			<tr>
				<td>4</td>
				<td>Matematika</td>
				<td>{{$row->nilai_pengetahuan_matematika}}</td>
				<td>{{$row->predikat_pengetahuan_matematika}}</td>
				<td>{{$row->deskripsi_pengetahuan_matematika}}</td>
				<td>{{$row->nilai_ketrampilan_matematika}}</td>
				<td>{{$row->predikat_ketrampilan_matematika}}</td>
				<td>{{$row->deskripsi_pengetahuan_matematika}}</td>
			</tr>
			<tr>
				<td>5</td>
				<td>Ilmu Pengetahuan Alam</td>
				<td>{{$row->nilai_pengetahuan_ipa}}</td>
				<td>{{$row->predikat_pengetahuan_ipa}}</td>
				<td>{{$row->deskripsi_pengetahuan_ipa}}</td>
				<td>{{$row->nilai_ketrampilan_ipa}}</td>
				<td>{{$row->predikat_ketrampilan_ipa}}</td>
				<td>{{$row->deskripsi_pengetahuan_ipa}}</td>
			</tr>
			<tr>
				<td>6</td>
				<td>Ilmu Pengetahuan Sosial</td>
				<td>{{$row->nilai_pengetahuan_ips}}</td>
				<td>{{$row->predikat_pengetahuan_ips}}</td>
				<td>{{$row->deskripsi_pengetahuan_ips}}</td>
				<td>{{$row->nilai_ketrampilan_ips}}</td>
				<td>{{$row->predikat_ketrampilan_ips}}</td>
				<td>{{$row->deskripsi_pengetahuan_ips}}</td>
			</tr>
			<tr>
				<td>7</td>
				<td>Seni Budaya dan Prakarya</td>
				<td>{{$row->nilai_pengetahuan_budaya}}</td>
				<td>{{$row->predikat_pengetahuan_budaya}}</td>
				<td>{{$row->deskripsi_pengetahuan_budaya}}</td>
				<td>{{$row->nilai_ketrampilan_budaya}}</td>
				<td>{{$row->predikat_ketrampilan_budaya}}</td>
				<td>{{$row->deskripsi_pengetahuan_budaya}}</td>
			</tr>
			<tr>
				<td>8</td>
				<td>Pendidikan Jasmani, Olahraga dan Kesehatan</td>
				<td>{{$row->nilai_pengetahuan_penjasorkes}}</td>
				<td>{{$row->predikat_pengetahuan_penjasorkes}}</td>
				<td>{{$row->deskripsi_pengetahuan_penjasorkes}}</td>
				<td>{{$row->nilai_ketrampilan_penjasorkes}}</td>
				<td>{{$row->predikat_ketrampilan_penjasorkes}}</td>
				<td>{{$row->deskripsi_pengetahuan_penjasorkes}}</td>
			</tr>
			<tr>
				<td>9</td>
				<td>Bahasa Daerah</td>
				<td>{{$row->nilai_pengetahuan_bahasadaerah}}</td>
				<td>{{$row->predikat_pengetahuan_bahasadaerah}}</td>
				<td>{{$row->deskripsi_pengetahuan_bahasadaerah}}</td>
				<td>{{$row->nilai_ketrampilan_bahasadaerah}}</td>
				<td>{{$row->predikat_ketrampilan_bahasadaerah}}</td>
				<td>{{$row->deskripsi_pengetahuan_bahasadaerah}}</td>
			</tr>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<br></br>
			<p><b>C.&nbsp;Ekstrakurikuler</b></p>
			<tr>
				<td>Kegiatan Ekstrakurikuler</td>
				<td>Keterangan</td>
			</tr>
			<tr>
				<td>{{$row->nama_ekstrakurikuler}}</td>
				<td>{{$row->keterangan_ekstrakurikuler}}</td>
			</tr>
		</table>
		<table style="text-align:left;" class="table table-bordered">
			<br></br>
			<p><b>D.&nbsp;Saran-saran</b></p>
			<tr>
				<td colspan="8" rowspan="2">{{$row->saran}}</td>
			</tr>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<br></br>
			<p><b>E.&nbsp;Perkembangan Fisik</b></p>
			<tr>
				<td colspan="1">No</td>
				<td colspan="2">Aspek yang dinilai</td>
				<td colspan="2">Semester</td>
			</tr>
			<tr>
				<td>1</td>
				<td colspan="2">Tinggi Badan</td>
				<td>{{$row->tinggibadan_s1}}</td>
				<td>{{$row->tinggibadan_s2}}</td>
			</tr>
			<tr>
				<td>2</td>
				<td colspan="2">Berat Badan</td>
				<td>{{$row->beratbadan_s1}}</td>
				<td>{{$row->beratbadan_s2}}</td>
			</tr>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<br></br>
			<p><b>F.&nbsp;Kondisi Kesehatan</b></p>
			<tr>
				<td>No</td>
				<td>Aspek Fisik</td>
				<td>Keterangan</td>
			</tr>
			<tr>
				<td>1</td>
				<td>Pendengaran</td>
				<td>{{$row->kondisi_pendengaran}}</td>
			</tr>
			<tr>
				<td>2</td>
				<td>Penglihatan</td>
				<td>{{$row->kondisi_penglihatan}}</td>
			</tr>
			<tr>
				<td>3</td>
				<td>Gigi</td>
				<td>{{$row->kondisi_gigi}}</td>
			</tr>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<br></br>
			<p><b>G.&nbsp;Catatan Prestasi</b></p>
			<tr>
				<td rowspan="2">No</td>
				<td colspan="2">Semester 1</td>
				<td colspan="2">Semester 2</td>
			</tr>
			<tr>
				<td>Jenis Prestasi</td>
				<td>Prestasi</td>
				<td>Jenis Prestasi</td>
				<td>Prestasi</td>
			</tr>
			<tr>
				<td>1</td>
				<td>{{$row->jenisprestasi_s1}}</td>
				<td>{{$row->prestasi_s1}}</td>
				<td>{{$row->jenisprestasi_s2}}</td>
				<td>{{$row->prestasi_s2}}</td>
			</tr>
			</tr>
		</table>
		<table style="text-align:center ;" class="table table-bordered">
			<br></br>
			<p><b>H.&nbsp;Ketidakhadiran</b></p>
			<tr>
				<td>Ketidakhadiran</td>
				<td>Hari</td>
			</tr>
			<tr>
				<td>Sakit</td>
				<td>{{$row->jumlah_sakit}}</td>
			</tr>
			<tr>
				<td>Izin</td>
				<td>{{$row->jumlah_izin}}</td>
			</tr>
			<tr>
				<td>Alpha</td>
				<td>{{$row->jumlah_alpha}}</td>
			</tr>
		</table>
		<br>
		<table class="table table-bordered">
			<tr>
				<td colspan="8"> <b>
						<p>Keputusan: <br></br> Berdasarkan Pencapaian Seluruh Kompetensi,
							Peserta Didik Dinyatakan: <br></br> {{$row->status}}&nbsp;{{$row->kelas_akhir}}
						</p>
					</b>

				</td>
			</tr>
		</table>
	</div>
</div>
</body>

@stop