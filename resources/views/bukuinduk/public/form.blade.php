

		 {!! Form::open(array('url'=>'bukuinduk', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Buku Induk</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Nama" class=" control-label col-md-4 "> Nama </label>
										<div class="col-md-8">
										  <input  type='text' name='nama' id='nama' value='{{ $row['nama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nipd" class=" control-label col-md-4 "> Nipd </label>
										<div class="col-md-8">
										  <input  type='text' name='nipd' id='nipd' value='{{ $row['nipd'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jk" class=" control-label col-md-4 "> Jk </label>
										<div class="col-md-8">
										  <input  type='text' name='jk' id='jk' value='{{ $row['jk'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nisn" class=" control-label col-md-4 "> Nisn </label>
										<div class="col-md-8">
										  <input  type='text' name='nisn' id='nisn' value='{{ $row['nisn'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tempat Lahir" class=" control-label col-md-4 "> Tempat Lahir </label>
										<div class="col-md-8">
										  <input  type='text' name='tempat_lahir' id='tempat_lahir' value='{{ $row['tempat_lahir'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tgl Lahir" class=" control-label col-md-4 "> Tgl Lahir </label>
										<div class="col-md-8">
										  
					{!! Form::text('tgl_lahir', $row['tgl_lahir'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nik" class=" control-label col-md-4 "> Nik </label>
										<div class="col-md-8">
										  <input  type='text' name='nik' id='nik' value='{{ $row['nik'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Agama" class=" control-label col-md-4 "> Agama </label>
										<div class="col-md-8">
										  <input  type='text' name='agama' id='agama' value='{{ $row['agama'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Alamat" class=" control-label col-md-4 "> Alamat </label>
										<div class="col-md-8">
										  <input  type='text' name='alamat' id='alamat' value='{{ $row['alamat'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Rt" class=" control-label col-md-4 "> Rt </label>
										<div class="col-md-8">
										  <input  type='text' name='rt' id='rt' value='{{ $row['rt'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Rw" class=" control-label col-md-4 "> Rw </label>
										<div class="col-md-8">
										  <input  type='text' name='rw' id='rw' value='{{ $row['rw'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Dusun" class=" control-label col-md-4 "> Dusun </label>
										<div class="col-md-8">
										  <input  type='text' name='dusun' id='dusun' value='{{ $row['dusun'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kelurahan" class=" control-label col-md-4 "> Kelurahan </label>
										<div class="col-md-8">
										  <input  type='text' name='kelurahan' id='kelurahan' value='{{ $row['kelurahan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kecamatan" class=" control-label col-md-4 "> Kecamatan </label>
										<div class="col-md-8">
										  <input  type='text' name='kecamatan' id='kecamatan' value='{{ $row['kecamatan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kode Pos" class=" control-label col-md-4 "> Kode Pos </label>
										<div class="col-md-8">
										  <input  type='text' name='kode_pos' id='kode_pos' value='{{ $row['kode_pos'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jenis Tinggal" class=" control-label col-md-4 "> Jenis Tinggal </label>
										<div class="col-md-8">
										  <input  type='text' name='jenis_tinggal' id='jenis_tinggal' value='{{ $row['jenis_tinggal'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Alat Transportasi" class=" control-label col-md-4 "> Alat Transportasi </label>
										<div class="col-md-8">
										  <input  type='text' name='alat_transportasi' id='alat_transportasi' value='{{ $row['alat_transportasi'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Telepon" class=" control-label col-md-4 "> Telepon </label>
										<div class="col-md-8">
										  <input  type='text' name='telepon' id='telepon' value='{{ $row['telepon'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Hp" class=" control-label col-md-4 "> Hp </label>
										<div class="col-md-8">
										  <input  type='text' name='hp' id='hp' value='{{ $row['hp'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Email" class=" control-label col-md-4 "> Email </label>
										<div class="col-md-8">
										  <input  type='text' name='email' id='email' value='{{ $row['email'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Skhun" class=" control-label col-md-4 "> Skhun </label>
										<div class="col-md-8">
										  <input  type='text' name='skhun' id='skhun' value='{{ $row['skhun'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Penerima Kps" class=" control-label col-md-4 "> Penerima Kps </label>
										<div class="col-md-8">
										  <input  type='text' name='penerima_kps' id='penerima_kps' value='{{ $row['penerima_kps'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Kps" class=" control-label col-md-4 "> No Kps </label>
										<div class="col-md-8">
										  <input  type='text' name='no_kps' id='no_kps' value='{{ $row['no_kps'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Ayah" class=" control-label col-md-4 "> Nama Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_ayah' id='nama_ayah' value='{{ $row['nama_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Thn Lahir Ayah" class=" control-label col-md-4 "> Thn Lahir Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='thn_lahir_ayah' id='thn_lahir_ayah' value='{{ $row['thn_lahir_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pendidikan Ayah" class=" control-label col-md-4 "> Pendidikan Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='pendidikan_ayah' id='pendidikan_ayah' value='{{ $row['pendidikan_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pekerjaan Ayah" class=" control-label col-md-4 "> Pekerjaan Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='pekerjaan_ayah' id='pekerjaan_ayah' value='{{ $row['pekerjaan_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Penghasilan Ayah" class=" control-label col-md-4 "> Penghasilan Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='penghasilan_ayah' id='penghasilan_ayah' value='{{ $row['penghasilan_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nik Ayah" class=" control-label col-md-4 "> Nik Ayah </label>
										<div class="col-md-8">
										  <input  type='text' name='nik_ayah' id='nik_ayah' value='{{ $row['nik_ayah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Ibu" class=" control-label col-md-4 "> Nama Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_ibu' id='nama_ibu' value='{{ $row['nama_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Thn Lahir Ibu" class=" control-label col-md-4 "> Thn Lahir Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='thn_lahir_ibu' id='thn_lahir_ibu' value='{{ $row['thn_lahir_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pendidikan Ibu" class=" control-label col-md-4 "> Pendidikan Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='pendidikan_ibu' id='pendidikan_ibu' value='{{ $row['pendidikan_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pekerjaan Ibu" class=" control-label col-md-4 "> Pekerjaan Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='pekerjaan_ibu' id='pekerjaan_ibu' value='{{ $row['pekerjaan_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Penghasilan Ibu" class=" control-label col-md-4 "> Penghasilan Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='penghasilan_ibu' id='penghasilan_ibu' value='{{ $row['penghasilan_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nik Ibu" class=" control-label col-md-4 "> Nik Ibu </label>
										<div class="col-md-8">
										  <input  type='text' name='nik_ibu' id='nik_ibu' value='{{ $row['nik_ibu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Wali" class=" control-label col-md-4 "> Nama Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_wali' id='nama_wali' value='{{ $row['nama_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Thn Lahir Wali" class=" control-label col-md-4 "> Thn Lahir Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='thn_lahir_wali' id='thn_lahir_wali' value='{{ $row['thn_lahir_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pendidikan Wali" class=" control-label col-md-4 "> Pendidikan Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='pendidikan_wali' id='pendidikan_wali' value='{{ $row['pendidikan_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Pekerjaan Wali" class=" control-label col-md-4 "> Pekerjaan Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='pekerjaan_wali' id='pekerjaan_wali' value='{{ $row['pekerjaan_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Penghasilan Wali" class=" control-label col-md-4 "> Penghasilan Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='penghasilan_wali' id='penghasilan_wali' value='{{ $row['penghasilan_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nik Wali" class=" control-label col-md-4 "> Nik Wali </label>
										<div class="col-md-8">
										  <input  type='text' name='nik_wali' id='nik_wali' value='{{ $row['nik_wali'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Rombel" class=" control-label col-md-4 "> Rombel </label>
										<div class="col-md-8">
										  <input  type='text' name='rombel' id='rombel' value='{{ $row['rombel'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Peserta Un" class=" control-label col-md-4 "> No Peserta Un </label>
										<div class="col-md-8">
										  <input  type='text' name='no_peserta_un' id='no_peserta_un' value='{{ $row['no_peserta_un'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Ijasah" class=" control-label col-md-4 "> No Ijasah </label>
										<div class="col-md-8">
										  <input  type='text' name='no_ijasah' id='no_ijasah' value='{{ $row['no_ijasah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Penerima Kip" class=" control-label col-md-4 "> Penerima Kip </label>
										<div class="col-md-8">
										  <input  type='text' name='penerima_kip' id='penerima_kip' value='{{ $row['penerima_kip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nomor Kip" class=" control-label col-md-4 "> Nomor Kip </label>
										<div class="col-md-8">
										  <input  type='text' name='nomor_kip' id='nomor_kip' value='{{ $row['nomor_kip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Kip" class=" control-label col-md-4 "> Nama Kip </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_kip' id='nama_kip' value='{{ $row['nama_kip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Kks" class=" control-label col-md-4 "> No Kks </label>
										<div class="col-md-8">
										  <input  type='text' name='no_kks' id='no_kks' value='{{ $row['no_kks'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Akta" class=" control-label col-md-4 "> No Akta </label>
										<div class="col-md-8">
										  <input  type='text' name='no_akta' id='no_akta' value='{{ $row['no_akta'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Bank" class=" control-label col-md-4 "> Bank </label>
										<div class="col-md-8">
										  <input  type='text' name='bank' id='bank' value='{{ $row['bank'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Rekening Bank" class=" control-label col-md-4 "> Rekening Bank </label>
										<div class="col-md-8">
										  <input  type='text' name='rekening_bank' id='rekening_bank' value='{{ $row['rekening_bank'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Rekening" class=" control-label col-md-4 "> Nama Rekening </label>
										<div class="col-md-8">
										  <input  type='text' name='nama_rekening' id='nama_rekening' value='{{ $row['nama_rekening'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Layak Pip" class=" control-label col-md-4 "> Layak Pip </label>
										<div class="col-md-8">
										  <input  type='text' name='layak_pip' id='layak_pip' value='{{ $row['layak_pip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Alasan Pip" class=" control-label col-md-4 "> Alasan Pip </label>
										<div class="col-md-8">
										  <input  type='text' name='alasan_pip' id='alasan_pip' value='{{ $row['alasan_pip'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Kebutuhan Khusus" class=" control-label col-md-4 "> Kebutuhan Khusus </label>
										<div class="col-md-8">
										  <input  type='text' name='kebutuhan_khusus' id='kebutuhan_khusus' value='{{ $row['kebutuhan_khusus'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Sekolah Asal" class=" control-label col-md-4 "> Sekolah Asal </label>
										<div class="col-md-8">
										  <input  type='text' name='sekolah_asal' id='sekolah_asal' value='{{ $row['sekolah_asal'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Anak Ke" class=" control-label col-md-4 "> Anak Ke </label>
										<div class="col-md-8">
										  <input  type='text' name='anak_ke' id='anak_ke' value='{{ $row['anak_ke'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Lintang" class=" control-label col-md-4 "> Lintang </label>
										<div class="col-md-8">
										  <input  type='text' name='lintang' id='lintang' value='{{ $row['lintang'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Bujur" class=" control-label col-md-4 "> Bujur </label>
										<div class="col-md-8">
										  <input  type='text' name='bujur' id='bujur' value='{{ $row['bujur'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="No Kk" class=" control-label col-md-4 "> No Kk </label>
										<div class="col-md-8">
										  <input  type='text' name='no_kk' id='no_kk' value='{{ $row['no_kk'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Berat Badan" class=" control-label col-md-4 "> Berat Badan </label>
										<div class="col-md-8">
										  <input  type='text' name='berat_badan' id='berat_badan' value='{{ $row['berat_badan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tinggi Badan" class=" control-label col-md-4 "> Tinggi Badan </label>
										<div class="col-md-8">
										  <input  type='text' name='tinggi_badan' id='tinggi_badan' value='{{ $row['tinggi_badan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Lingkar Kepala" class=" control-label col-md-4 "> Lingkar Kepala </label>
										<div class="col-md-8">
										  <input  type='text' name='lingkar_kepala' id='lingkar_kepala' value='{{ $row['lingkar_kepala'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jumlah Saudara Kandung" class=" control-label col-md-4 "> Jumlah Saudara Kandung </label>
										<div class="col-md-8">
										  <input  type='text' name='jumlah_saudara_kandung' id='jumlah_saudara_kandung' value='{{ $row['jumlah_saudara_kandung'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Jarak Rumah" class=" control-label col-md-4 "> Jarak Rumah </label>
										<div class="col-md-8">
										  <input  type='text' name='jarak_rumah' id='jarak_rumah' value='{{ $row['jarak_rumah'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Status" class=" control-label col-md-4 "> Status </label>
										<div class="col-md-8">
										  <input  type='text' name='status' id='status' value='{{ $row['status'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
