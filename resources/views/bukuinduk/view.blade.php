@extends('layouts.app')

@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>
<div class="card">
	<div class="card-body">

		<div class="toolbar-nav">
			<div class="row">
				<div class="col-md-6 ">
					<div class="btn-group">
						<a href="{{ url('bukuinduk?return='.$return) }}" class="tips btn btn-danger  btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
						@if($access['is_add'] ==1)
				   		<a href="{{ url('bukuinduk/'.$id.'/edit?return='.$return) }}" class="tips btn btn-info btn-sm  " title="{{ __('core.btn_edit') }}"><i class="icon-note"></i></a>
						@endif
					</div>	
				</div>
				<div class="col-md-6 text-right">			
					<div class="btn-group">
				   		<a href="{{ ($prevnext['prev'] != '' ? url('bukuinduk/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-primary  btn-sm"><i class="fa fa-arrow-left"></i>  </a>	
						<a href="{{ ($prevnext['next'] != '' ? url('bukuinduk/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-primary btn-sm "> <i class="fa fa-arrow-right"></i>  </a>			
					</div>			
				</div>	

				
				
			</div>
		</div>
	
		<div class="table-responsive">
			<table class="table  table-bordered " >
				<tbody>	
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama', (isset($fields['nama']['language'])? $fields['nama']['language'] : array())) }}</td>
						<td>{{ $row->nama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nipd', (isset($fields['nipd']['language'])? $fields['nipd']['language'] : array())) }}</td>
						<td>{{ $row->nipd}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jk', (isset($fields['jk']['language'])? $fields['jk']['language'] : array())) }}</td>
						<td>{{ $row->jk}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nisn', (isset($fields['nisn']['language'])? $fields['nisn']['language'] : array())) }}</td>
						<td>{{ $row->nisn}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tempat Lahir', (isset($fields['tempat_lahir']['language'])? $fields['tempat_lahir']['language'] : array())) }}</td>
						<td>{{ $row->tempat_lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tgl Lahir', (isset($fields['tgl_lahir']['language'])? $fields['tgl_lahir']['language'] : array())) }}</td>
						<td>{{ $row->tgl_lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nik', (isset($fields['nik']['language'])? $fields['nik']['language'] : array())) }}</td>
						<td>{{ $row->nik}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Agama', (isset($fields['agama']['language'])? $fields['agama']['language'] : array())) }}</td>
						<td>{{ $row->agama}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alamat', (isset($fields['alamat']['language'])? $fields['alamat']['language'] : array())) }}</td>
						<td>{{ $row->alamat}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rt', (isset($fields['rt']['language'])? $fields['rt']['language'] : array())) }}</td>
						<td>{{ $row->rt}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rw', (isset($fields['rw']['language'])? $fields['rw']['language'] : array())) }}</td>
						<td>{{ $row->rw}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Dusun', (isset($fields['dusun']['language'])? $fields['dusun']['language'] : array())) }}</td>
						<td>{{ $row->dusun}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kelurahan', (isset($fields['kelurahan']['language'])? $fields['kelurahan']['language'] : array())) }}</td>
						<td>{{ $row->kelurahan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kecamatan', (isset($fields['kecamatan']['language'])? $fields['kecamatan']['language'] : array())) }}</td>
						<td>{{ $row->kecamatan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kode Pos', (isset($fields['kode_pos']['language'])? $fields['kode_pos']['language'] : array())) }}</td>
						<td>{{ $row->kode_pos}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenis Tinggal', (isset($fields['jenis_tinggal']['language'])? $fields['jenis_tinggal']['language'] : array())) }}</td>
						<td>{{ $row->jenis_tinggal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alat Transportasi', (isset($fields['alat_transportasi']['language'])? $fields['alat_transportasi']['language'] : array())) }}</td>
						<td>{{ $row->alat_transportasi}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Telepon', (isset($fields['telepon']['language'])? $fields['telepon']['language'] : array())) }}</td>
						<td>{{ $row->telepon}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Hp', (isset($fields['hp']['language'])? $fields['hp']['language'] : array())) }}</td>
						<td>{{ $row->hp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Email', (isset($fields['email']['language'])? $fields['email']['language'] : array())) }}</td>
						<td>{{ $row->email}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Skhun', (isset($fields['skhun']['language'])? $fields['skhun']['language'] : array())) }}</td>
						<td>{{ $row->skhun}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Penerima Kps', (isset($fields['penerima_kps']['language'])? $fields['penerima_kps']['language'] : array())) }}</td>
						<td>{{ $row->penerima_kps}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Kps', (isset($fields['no_kps']['language'])? $fields['no_kps']['language'] : array())) }}</td>
						<td>{{ $row->no_kps}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Ayah', (isset($fields['nama_ayah']['language'])? $fields['nama_ayah']['language'] : array())) }}</td>
						<td>{{ $row->nama_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Thn Lahir Ayah', (isset($fields['thn_lahir_ayah']['language'])? $fields['thn_lahir_ayah']['language'] : array())) }}</td>
						<td>{{ $row->thn_lahir_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pendidikan Ayah', (isset($fields['pendidikan_ayah']['language'])? $fields['pendidikan_ayah']['language'] : array())) }}</td>
						<td>{{ $row->pendidikan_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pekerjaan Ayah', (isset($fields['pekerjaan_ayah']['language'])? $fields['pekerjaan_ayah']['language'] : array())) }}</td>
						<td>{{ $row->pekerjaan_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Penghasilan Ayah', (isset($fields['penghasilan_ayah']['language'])? $fields['penghasilan_ayah']['language'] : array())) }}</td>
						<td>{{ $row->penghasilan_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nik Ayah', (isset($fields['nik_ayah']['language'])? $fields['nik_ayah']['language'] : array())) }}</td>
						<td>{{ $row->nik_ayah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Ibu', (isset($fields['nama_ibu']['language'])? $fields['nama_ibu']['language'] : array())) }}</td>
						<td>{{ $row->nama_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Thn Lahir Ibu', (isset($fields['thn_lahir_ibu']['language'])? $fields['thn_lahir_ibu']['language'] : array())) }}</td>
						<td>{{ $row->thn_lahir_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pendidikan Ibu', (isset($fields['pendidikan_ibu']['language'])? $fields['pendidikan_ibu']['language'] : array())) }}</td>
						<td>{{ $row->pendidikan_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pekerjaan Ibu', (isset($fields['pekerjaan_ibu']['language'])? $fields['pekerjaan_ibu']['language'] : array())) }}</td>
						<td>{{ $row->pekerjaan_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Penghasilan Ibu', (isset($fields['penghasilan_ibu']['language'])? $fields['penghasilan_ibu']['language'] : array())) }}</td>
						<td>{{ $row->penghasilan_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nik Ibu', (isset($fields['nik_ibu']['language'])? $fields['nik_ibu']['language'] : array())) }}</td>
						<td>{{ $row->nik_ibu}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Wali', (isset($fields['nama_wali']['language'])? $fields['nama_wali']['language'] : array())) }}</td>
						<td>{{ $row->nama_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Thn Lahir Wali', (isset($fields['thn_lahir_wali']['language'])? $fields['thn_lahir_wali']['language'] : array())) }}</td>
						<td>{{ $row->thn_lahir_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pendidikan Wali', (isset($fields['pendidikan_wali']['language'])? $fields['pendidikan_wali']['language'] : array())) }}</td>
						<td>{{ $row->pendidikan_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Pekerjaan Wali', (isset($fields['pekerjaan_wali']['language'])? $fields['pekerjaan_wali']['language'] : array())) }}</td>
						<td>{{ $row->pekerjaan_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Penghasilan Wali', (isset($fields['penghasilan_wali']['language'])? $fields['penghasilan_wali']['language'] : array())) }}</td>
						<td>{{ $row->penghasilan_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nik Wali', (isset($fields['nik_wali']['language'])? $fields['nik_wali']['language'] : array())) }}</td>
						<td>{{ $row->nik_wali}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rombel', (isset($fields['rombel']['language'])? $fields['rombel']['language'] : array())) }}</td>
						<td>{{ $row->rombel}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Peserta Un', (isset($fields['no_peserta_un']['language'])? $fields['no_peserta_un']['language'] : array())) }}</td>
						<td>{{ $row->no_peserta_un}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Ijasah', (isset($fields['no_ijasah']['language'])? $fields['no_ijasah']['language'] : array())) }}</td>
						<td>{{ $row->no_ijasah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Penerima Kip', (isset($fields['penerima_kip']['language'])? $fields['penerima_kip']['language'] : array())) }}</td>
						<td>{{ $row->penerima_kip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nomor Kip', (isset($fields['nomor_kip']['language'])? $fields['nomor_kip']['language'] : array())) }}</td>
						<td>{{ $row->nomor_kip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Kip', (isset($fields['nama_kip']['language'])? $fields['nama_kip']['language'] : array())) }}</td>
						<td>{{ $row->nama_kip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Kks', (isset($fields['no_kks']['language'])? $fields['no_kks']['language'] : array())) }}</td>
						<td>{{ $row->no_kks}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Akta', (isset($fields['no_akta']['language'])? $fields['no_akta']['language'] : array())) }}</td>
						<td>{{ $row->no_akta}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Bank', (isset($fields['bank']['language'])? $fields['bank']['language'] : array())) }}</td>
						<td>{{ $row->bank}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Rekening Bank', (isset($fields['rekening_bank']['language'])? $fields['rekening_bank']['language'] : array())) }}</td>
						<td>{{ $row->rekening_bank}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Rekening', (isset($fields['nama_rekening']['language'])? $fields['nama_rekening']['language'] : array())) }}</td>
						<td>{{ $row->nama_rekening}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Layak Pip', (isset($fields['layak_pip']['language'])? $fields['layak_pip']['language'] : array())) }}</td>
						<td>{{ $row->layak_pip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Alasan Pip', (isset($fields['alasan_pip']['language'])? $fields['alasan_pip']['language'] : array())) }}</td>
						<td>{{ $row->alasan_pip}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Kebutuhan Khusus', (isset($fields['kebutuhan_khusus']['language'])? $fields['kebutuhan_khusus']['language'] : array())) }}</td>
						<td>{{ $row->kebutuhan_khusus}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sekolah Asal', (isset($fields['sekolah_asal']['language'])? $fields['sekolah_asal']['language'] : array())) }}</td>
						<td>{{ $row->sekolah_asal}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Anak Ke', (isset($fields['anak_ke']['language'])? $fields['anak_ke']['language'] : array())) }}</td>
						<td>{{ $row->anak_ke}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lintang', (isset($fields['lintang']['language'])? $fields['lintang']['language'] : array())) }}</td>
						<td>{{ $row->lintang}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Bujur', (isset($fields['bujur']['language'])? $fields['bujur']['language'] : array())) }}</td>
						<td>{{ $row->bujur}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('No Kk', (isset($fields['no_kk']['language'])? $fields['no_kk']['language'] : array())) }}</td>
						<td>{{ $row->no_kk}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Berat Badan', (isset($fields['berat_badan']['language'])? $fields['berat_badan']['language'] : array())) }}</td>
						<td>{{ $row->berat_badan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tinggi Badan', (isset($fields['tinggi_badan']['language'])? $fields['tinggi_badan']['language'] : array())) }}</td>
						<td>{{ $row->tinggi_badan}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lingkar Kepala', (isset($fields['lingkar_kepala']['language'])? $fields['lingkar_kepala']['language'] : array())) }}</td>
						<td>{{ $row->lingkar_kepala}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jumlah Saudara Kandung', (isset($fields['jumlah_saudara_kandung']['language'])? $fields['jumlah_saudara_kandung']['language'] : array())) }}</td>
						<td>{{ $row->jumlah_saudara_kandung}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jarak Rumah', (isset($fields['jarak_rumah']['language'])? $fields['jarak_rumah']['language'] : array())) }}</td>
						<td>{{ $row->jarak_rumah}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) }}</td>
						<td>{{ $row->status}} </td>
						
					</tr>
				
				</tbody>	
			</table>   

		 	

		</div>
			
	</div>
</div>		
@stop
