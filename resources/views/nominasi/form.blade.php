@extends('layouts.app')

@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>
<div class="card">
	<div class="card-body">


	{!! Form::open(array('url'=>'nominasi?return='.$return, 'class'=>'form-horizontal  validated sximo-form','files' => true ,'id'=> 'FormTable' )) !!}
	<div class="toolbar-nav">
		<div class="row">
			<div class="col-md-6 " >
				 <a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-danger  btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a>
			</div>
			<div class="col-md-6  text-right " >
				<div class="btn-group">
					
						<button name="apply" class="tips btn btn-sm btn-info  "  title="{{ __('core.btn_back') }}" > {{ __('core.sb_apply') }} </button>
						<button name="save" class="tips btn btn-sm btn-primary "  id="saved-button" title="{{ __('core.btn_back') }}" > {{ __('core.sb_save') }} </button> 
						
					
				</div>		
			</div>
			
		</div>
	</div>	


	
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
	<div class="">
		<div class="col-md-12">
						<fieldset><legend> m_nominasitetap</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Nomor Peserta US" class=" control-label col-md-4 "> Nomor Peserta US </label>
										<div class="col-md-8">
										  <input  type='text' name='Nomor_Peserta_US' id='Nomor_Peserta_US' value='{{ $row['Nomor_Peserta_US'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="NISN" class=" control-label col-md-4 "> NISN </label>
										<div class="col-md-8">
										  <input  type='text' name='NISN' id='NISN' value='{{ $row['NISN'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Par" class=" control-label col-md-4 "> Par </label>
										<div class="col-md-8">
										  <input  type='text' name='Par' id='Par' value='{{ $row['Par'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Abs" class=" control-label col-md-4 "> Abs </label>
										<div class="col-md-8">
										  <input  type='text' name='Abs' id='Abs' value='{{ $row['Abs'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Siswa" class=" control-label col-md-4 "> Nama Siswa </label>
										<div class="col-md-8">
										  <input  type='text' name='Nama_Siswa' id='Nama_Siswa' value='{{ $row['Nama_Siswa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="L P" class=" control-label col-md-4 "> L P </label>
										<div class="col-md-8">
										  
					<?php $L_P = explode(',',$row['L_P']);
					$L_P_opt = array( 'L' => 'L' ,  'P' => 'P' , ); ?>
					<select name='L_P' rows='5'   class='select2 '  > 
						<?php 
						foreach($L_P_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['L_P'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tempat Lahir" class=" control-label col-md-4 "> Tempat Lahir </label>
										<div class="col-md-8">
										  <input  type='text' name='Tempat_Lahir' id='Tempat_Lahir' value='{{ $row['Tempat_Lahir'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tanggal Lahir" class=" control-label col-md-4 "> Tanggal Lahir </label>
										<div class="col-md-8">
										  
					{!! Form::text('Tanggal_Lahir', $row['Tanggal_Lahir'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Ortu" class=" control-label col-md-4 "> Nama Ortu </label>
										<div class="col-md-8">
										  <input  type='text' name='Nama_Ortu' id='Nama_Ortu' value='{{ $row['Nama_Ortu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> </fieldset></div>

	</div>
	
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 	
		 	 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("nominasi/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop