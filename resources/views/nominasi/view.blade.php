@extends('layouts.app')

@section('content')
<div class="page-titles">
  <h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
</div>
<div class="card">
	<div class="card-body">

		<div class="toolbar-nav">
			<div class="row">
				<div class="col-md-6 ">
					<div class="btn-group">
						<a href="{{ url('nominasi?return='.$return) }}" class="tips btn btn-danger  btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
						@if($access['is_add'] ==1)
				   		<a href="{{ url('nominasi/'.$id.'/edit?return='.$return) }}" class="tips btn btn-info btn-sm  " title="{{ __('core.btn_edit') }}"><i class="icon-note"></i></a>
						@endif
					</div>	
				</div>
				<div class="col-md-6 text-right">			
					<div class="btn-group">
				   		<a href="{{ ($prevnext['prev'] != '' ? url('nominasi/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-primary  btn-sm"><i class="fa fa-arrow-left"></i>  </a>	
						<a href="{{ ($prevnext['next'] != '' ? url('nominasi/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-primary btn-sm "> <i class="fa fa-arrow-right"></i>  </a>			
					</div>			
				</div>	

				
				
			</div>
		</div>
	
		<div class="table-responsive">
			<table class="table  table-bordered " >
				<tbody>	
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nomor Peserta US', (isset($fields['Nomor_Peserta_US']['language'])? $fields['Nomor_Peserta_US']['language'] : array())) }}</td>
						<td>{{ $row->Nomor_Peserta_US}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NISN', (isset($fields['NISN']['language'])? $fields['NISN']['language'] : array())) }}</td>
						<td>{{ $row->NISN}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('PAR', (isset($fields['Par']['language'])? $fields['Par']['language'] : array())) }}</td>
						<td>{{ $row->Par}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ABS', (isset($fields['Abs']['language'])? $fields['Abs']['language'] : array())) }}</td>
						<td>{{ $row->Abs}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Siswa', (isset($fields['Nama_Siswa']['language'])? $fields['Nama_Siswa']['language'] : array())) }}</td>
						<td>{{ $row->Nama_Siswa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Jenis Kelamin', (isset($fields['L_P']['language'])? $fields['L_P']['language'] : array())) }}</td>
						<td>{{ $row->L_P}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tempat Lahir', (isset($fields['Tempat_Lahir']['language'])? $fields['Tempat_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tempat_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tanggal Lahir', (isset($fields['Tanggal_Lahir']['language'])? $fields['Tanggal_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tanggal_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Orangtua', (isset($fields['Nama_Ortu']['language'])? $fields['Nama_Ortu']['language'] : array())) }}</td>
						<td>{{ $row->Nama_Ortu}} </td>
						
					</tr>
				
				</tbody>	
			</table>   

		 	

		</div>
			
	</div>
</div>		
@stop
