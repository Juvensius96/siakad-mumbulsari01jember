

		 {!! Form::open(array('url'=>'nominasi', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> m_nominasitetap</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group row  " >
										<label for="Nomor Peserta US" class=" control-label col-md-4 "> Nomor Peserta US </label>
										<div class="col-md-8">
										  <input  type='text' name='Nomor_Peserta_US' id='Nomor_Peserta_US' value='{{ $row['Nomor_Peserta_US'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="NISN" class=" control-label col-md-4 "> NISN </label>
										<div class="col-md-8">
										  <input  type='text' name='NISN' id='NISN' value='{{ $row['NISN'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Par" class=" control-label col-md-4 "> Par </label>
										<div class="col-md-8">
										  <input  type='text' name='Par' id='Par' value='{{ $row['Par'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Abs" class=" control-label col-md-4 "> Abs </label>
										<div class="col-md-8">
										  <input  type='text' name='Abs' id='Abs' value='{{ $row['Abs'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Siswa" class=" control-label col-md-4 "> Nama Siswa </label>
										<div class="col-md-8">
										  <input  type='text' name='Nama_Siswa' id='Nama_Siswa' value='{{ $row['Nama_Siswa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="L P" class=" control-label col-md-4 "> L P </label>
										<div class="col-md-8">
										  <input  type='text' name='L_P' id='L_P' value='{{ $row['L_P'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tempat Lahir" class=" control-label col-md-4 "> Tempat Lahir </label>
										<div class="col-md-8">
										  <input  type='text' name='Tempat_Lahir' id='Tempat_Lahir' value='{{ $row['Tempat_Lahir'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tanggal Lahir" class=" control-label col-md-4 "> Tanggal Lahir </label>
										<div class="col-md-8">
										  
					{!! Form::text('Tanggal_Lahir', $row['Tanggal_Lahir'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Nama Ortu" class=" control-label col-md-4 "> Nama Ortu </label>
										<div class="col-md-8">
										  <input  type='text' name='Nama_Ortu' id='Nama_Ortu' value='{{ $row['Nama_Ortu'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
