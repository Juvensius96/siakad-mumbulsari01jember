<div class="container" class="pt-3 pb-3">
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-container" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nomor Peserta US', (isset($fields['Nomor_Peserta_US']['language'])? $fields['Nomor_Peserta_US']['language'] : array())) }}</td>
						<td>{{ $row->Nomor_Peserta_US}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('NISN', (isset($fields['NISN']['language'])? $fields['NISN']['language'] : array())) }}</td>
						<td>{{ $row->NISN}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Par', (isset($fields['Par']['language'])? $fields['Par']['language'] : array())) }}</td>
						<td>{{ $row->Par}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Abs', (isset($fields['Abs']['language'])? $fields['Abs']['language'] : array())) }}</td>
						<td>{{ $row->Abs}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Siswa', (isset($fields['Nama_Siswa']['language'])? $fields['Nama_Siswa']['language'] : array())) }}</td>
						<td>{{ $row->Nama_Siswa}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('L P', (isset($fields['L_P']['language'])? $fields['L_P']['language'] : array())) }}</td>
						<td>{{ $row->L_P}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tempat Lahir', (isset($fields['Tempat_Lahir']['language'])? $fields['Tempat_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tempat_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tanggal Lahir', (isset($fields['Tanggal_Lahir']['language'])? $fields['Tanggal_Lahir']['language'] : array())) }}</td>
						<td>{{ $row->Tanggal_Lahir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Nama Ortu', (isset($fields['Nama_Ortu']['language'])? $fields['Nama_Ortu']['language'] : array())) }}</td>
						<td>{{ $row->Nama_Ortu}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	