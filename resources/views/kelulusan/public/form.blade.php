

		 {!! Form::open(array('url'=>'kelulusan', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Kelulusan</legend>
				{!! Form::hidden('id_kelulusan', $row['id_kelulusan']) !!}					
									  <div class="form-group row  " >
										<label for="Id Siswa" class=" control-label col-md-4 "> Id Siswa </label>
										<div class="col-md-8">
										  <input  type='text' name='id_siswa' id='id_siswa' value='{{ $row['id_siswa'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Id Statuskelulusan" class=" control-label col-md-4 "> Id Statuskelulusan </label>
										<div class="col-md-8">
										  <input  type='text' name='id_statuskelulusan' id='id_statuskelulusan' value='{{ $row['id_statuskelulusan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tahun Masuk" class=" control-label col-md-4 "> Tahun Masuk </label>
										<div class="col-md-8">
										  <input  type='text' name='tahun_masuk' id='tahun_masuk' value='{{ $row['tahun_masuk'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Tahun Kelulusan" class=" control-label col-md-4 "> Tahun Kelulusan </label>
										<div class="col-md-8">
										  <input  type='text' name='tahun_kelulusan' id='tahun_kelulusan' value='{{ $row['tahun_kelulusan'] }}' 
						     class='form-control form-control-sm ' /> 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Skhun" class=" control-label col-md-4 "> Skhun </label>
										<div class="col-md-8">
										  
						<div class="fileUpload btn " > 
						    <span>  <i class="fa fa-camera"></i>  </span>
						    <div class="title"> Browse File </div>
						    <input type="file" name="skhun" class="upload"   accept="image/x-png,image/gif,image/jpeg"     />
						</div>
						<div class="skhun-preview preview-upload">
							{!! SiteHelpers::showUploadedFile( $row["skhun"],"/uploads/images/skhun") !!}
						</div>
					 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Ijasah" class=" control-label col-md-4 "> Ijasah </label>
										<div class="col-md-8">
										  
						<div class="fileUpload btn " > 
						    <span>  <i class="fa fa-camera"></i>  </span>
						    <div class="title"> Browse File </div>
						    <input type="file" name="ijasah" class="upload"   accept="image/x-png,image/gif,image/jpeg"     />
						</div>
						<div class="ijasah-preview preview-upload">
							{!! SiteHelpers::showUploadedFile( $row["ijasah"],"/uploads/images/ijasah") !!}
						</div>
					 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Updated At" class=" control-label col-md-4 "> Updated At </label>
										<div class="col-md-8">
										  
					{!! Form::text('updated_at', $row['updated_at'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 
									  </div> 					
									  <div class="form-group row  " >
										<label for="Created At" class=" control-label col-md-4 "> Created At </label>
										<div class="col-md-8">
										  
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control form-control-sm datetime')) !!}
				 
										 </div> 
										 
									  </div> </fieldset></div>

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-default btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-default btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
