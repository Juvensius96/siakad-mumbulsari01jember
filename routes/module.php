<?php
        
// Start Routes for pendidik 
Route::resource('pendidik','PendidikController');
// End Routes for pendidik 

                    
// Start Routes for nominasi 
Route::resource('nominasi','NominasiController');
// End Routes for nominasi 

                    
// Start Routes for kelulusan 
Route::resource('kelulusan','KelulusanController');
// End Routes for kelulusan 

                    
// Start Routes for bukuinduk 
Route::resource('bukuinduk','BukuindukController');
// End Routes for bukuinduk 

                    
// Start Routes for raport 
Route::resource('raport','RaportController');
// End Routes for raport 

                    ?>