<?php
        
// Start Routes for pendidik 
Route::resource('services/pendidik','Services\PendidikController');
// End Routes for pendidik 

                    
// Start Routes for nominasi 
Route::resource('services/nominasi','Services\NominasiController');
// End Routes for nominasi 

                    
// Start Routes for kelulusan 
Route::resource('services/kelulusan','Services\KelulusanController');
// End Routes for kelulusan 

                    
// Start Routes for bukuinduk 
Route::resource('services/bukuinduk','Services\BukuindukController');
// End Routes for bukuinduk 

                    
// Start Routes for raport 
Route::resource('services/raport','Services\RaportController');
// End Routes for raport 

                    ?>