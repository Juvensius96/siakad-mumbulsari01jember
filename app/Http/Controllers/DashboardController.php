<?php namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;

class DashboardController extends Controller {

	public function __construct()
	{
		parent::__construct();
		
        $this->data = array(
            'pageTitle' =>  $this->config['cnf_appname'],
            'pageNote'  =>  'Welcome to Dashboard',
            
        );			
	}

	public function index( Request $request )
	{
		$tb_murid = \DB::table('tb_murid')->get();
		$this->data['count_murid'] =  $tb_murid->count();

		$tb_pendidik = \DB::table('tb_pendidik')->get();
		$this->data['count_ptk'] = $tb_pendidik->count();

		$tb_nominasitetap = \DB::table('tb_nominasitetap')->get();
		$this->data['count_dnt'] = $tb_nominasitetap->count();

		$tb_kelulusan =\DB::table('tb_kelulusan')->get();
		$this->data['count_kelulusan'] = $tb_kelulusan->count();
		
		return view('dashboard.index',$this->data);
	}	


	


}