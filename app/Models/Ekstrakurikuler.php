<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class ekstrakurikuler extends Sximo  {
	
	protected $table = 'tb_ekstrakurikuler';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_ekstrakurikuler.* FROM tb_ekstrakurikuler  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_ekstrakurikuler.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
