<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class nominasi extends Sximo  {
	
	protected $table = 'tb_nominasitetap';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_nominasitetap.* FROM tb_nominasitetap  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_nominasitetap.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
