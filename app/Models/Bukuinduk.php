<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bukuinduk extends Sximo  {
	
	protected $table = 'tb_murid';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_murid.* FROM tb_murid  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_murid.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
