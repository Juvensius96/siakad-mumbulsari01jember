<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class kelulusan extends Sximo  {
	
	protected $table = 'tb_kelulusan';
	protected $primaryKey = 'id_kelulusan';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT tb_kelulusan.* FROM tb_kelulusan  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE tb_kelulusan.id_kelulusan IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
